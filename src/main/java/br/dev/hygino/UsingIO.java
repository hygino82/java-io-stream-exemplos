package br.dev.hygino;

import java.io.*;

public class UsingIO {
    private static final String MY_FILE = "e:" + File.separator + "teste-arquivo.txt";

    public UsingIO() {
       /* try {
            writeFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }*/
        readFile();
    }

    private void readFile() {
        //leitura de arquivos sem try-with-resources
       /* InputStream input = null;
        try {
            input = new FileInputStream(MY_FILE);
            int content;

            while ((content = input.read()) != -1) {
                System.out.print((char) content);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }*/

       try (final InputStream input = new FileInputStream(MY_FILE)) {
            int content;
            while ((content = input.read()) != -1) {
                System.out.print((char) content);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        /*
        try (final FileReader reader = new FileReader(MY_FILE)) {
            int content = reader.read();
            while (content != -1) {
                System.out.print((char) content);
                content = reader.read();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }*/
    }

    private void writeFile() {
       /* final File file = new File(MY_FILE);
        boolean fileIsCreated = false;

        if (!file.exists()) {
            fileIsCreated = file.createNewFile();
        }

        if (fileIsCreated || file.exists()) {*/

        /*final OutputStream output = new FileOutputStream(MY_FILE);

        output.write("Olá mundo IO".getBytes("UTF-8"));
        output.close();

        System.out.println("Dados gravados no arquivo");
        */
        //}

    }

    public static void main(String[] args) {
        new UsingIO();
    }
}
