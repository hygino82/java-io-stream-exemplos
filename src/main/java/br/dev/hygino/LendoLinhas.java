package br.dev.hygino;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LendoLinhas {
    public static void main(String[] args) {
        char c;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            String str;
            System.out.println("Insira linhas de texto.");
            System.out.println("Digite 'parar' para sair");
            do {
                str = br.readLine();
                System.out.println(str);
            } while (!str.equals("parar"));

        } catch (IOException exc) {
            System.out.println("Ocorreu um erro na leitura!");
        }
    }
}
