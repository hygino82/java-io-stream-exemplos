package br.dev.hygino;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class TecladoParaArquivo {
    public static void main(String[] args) {
        String str;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); FileWriter fw = new FileWriter("test.txt")) {
            System.out.println("Insira linhas de texto.");
            System.out.println("Digite 'parar' para sair");
            do {
                System.out.print(": ");
                str = br.readLine();
                if (str.compareTo("parar") == 0) break;
                str = str + "\r\n";
                fw.write(str);

            } while (str.compareTo("parar") != 0);

        } catch (IOException exc) {
            System.out.println("Ocorreu um erro!");
        }
    }
}
