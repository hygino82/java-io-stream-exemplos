package br.dev.hygino;

import java.io.*;

public class LerGravarDados {
    public static void main(String[] args) {
        int i = 10;
        double d = 1023.56;
        boolean b = true;

        try (DataOutputStream dataOut = new DataOutputStream(new FileOutputStream("testdata"))) {
            System.out.println("Escrevendo: " + i);
            dataOut.writeInt(i);

            System.out.println("Escrevendo: " + b);
            dataOut.writeBoolean(b);

            System.out.println("Escrevendo: " + d);
            dataOut.writeDouble(d);

            System.out.println("Escrevendo: " + 12.2 * 7.4);
            dataOut.writeDouble(12.2 * 7.4);
        } catch (IOException exc) {
            System.out.println("Erro de escrita!");
            return;
        }

        System.out.println("-------------------------");

        try (DataInputStream dataIn = new DataInputStream(new FileInputStream("testdata"))) {
            i = dataIn.readInt();
            System.out.println("Lendo: " + i);

            b = dataIn.readBoolean();
            System.out.println("Lendo: " + b);

            d = dataIn.readDouble();
            System.out.println("Lendo: " + d);

            d = dataIn.readDouble();
            System.out.println("Lendo: " + d);

        } catch (IOException exc) {
            System.out.println("Erro de leitura!");
            return;
        }
    }
}
