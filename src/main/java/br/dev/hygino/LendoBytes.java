package br.dev.hygino;

import java.io.*;

public class LendoBytes {
    public static void main(String[] args) {
        byte data[] = new byte[10];
        System.out.println("Informe alguns caracteres");
        try {
            System.in.read(data);
            System.out.print("Você informou: ");
            for (int i = 0; i < data.length; i++) {
                System.out.print((char) data[i]);
            }
        } catch (IOException e) {
            System.out.println("Ocorreu elgum erro na leitura");
        }
    }
}
