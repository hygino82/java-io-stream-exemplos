package br.dev.hygino;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UsingNIO2 {
    private static final String MY_FILE = "e:" + File.separator + "teste-arquivo-nio2.txt";

    public UsingNIO2() {
        try {
            readFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("------------------------------------");
        try {
            writeFile();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void readFile() throws IOException {
        final Path path = Paths.get(MY_FILE);
        final var writtenPath = Files.writeString(path, "Olá mundo NIO2");

        System.out.println("Arquivo Gravado com sucesso!");
    }

    private void writeFile() throws IOException {
        final Path path = Paths.get(MY_FILE);
        //System.out.println();//array das linhas é lido abaixo
        Files.readAllLines(path).forEach(System.out::print);
    }

    public static void main(String[] args) {
        new UsingNIO2();
    }
}
