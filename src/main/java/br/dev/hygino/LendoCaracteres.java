package br.dev.hygino;

import java.io.*;

public class LendoCaracteres {
    public static void main(String[] args) {
        char c;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Informe caracteres digite . para terminar");
            do {
                c = (char) br.read();
                System.out.println(c);
            } while (c != '.');

        } catch (IOException exc) {
            System.out.println("Ocorreu um erro na leitura!");
        }
    }
}
