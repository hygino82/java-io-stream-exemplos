package br.dev.hygino;

import java.io.*;

public class MediaAritmetica {
    public static void main(String[] args) {

        String str;
        int n;
        double soma = 0.0;
        double media, t;

        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Quantos números serão informados: ");
            try {
                str = br.readLine();
                n = Integer.parseInt(str);
            } catch (NumberFormatException nfe) {
                System.out.println("Erro ao converter String para Número");
                n = 0;
            }

            System.out.println("Enter " + n + " values.");
            try {
                for (int i = 0; i < n; i++) {
                    System.out.print(": ");
                    str = br.readLine();
                    t = Double.parseDouble(str);
                    soma += t;
                }
                media = soma / n;
                System.out.println("Média " + media);
            } catch (NumberFormatException nfe) {
                System.out.println("Erro ao converter String para Número");
                //t = 0.0;
            }
        } catch (IOException exc) {
            System.out.println("Erro de leitura!");
        }
    }
}
