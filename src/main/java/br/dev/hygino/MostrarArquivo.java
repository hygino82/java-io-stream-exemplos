package br.dev.hygino;

import java.io.*;

public class MostrarArquivo {
    public static void main(String[] args) {
        int i;
        String nomeArquivo = "xanadu.txt";
        System.out.println("Usage: ShowFile File");

        try (FileInputStream fin = new FileInputStream(nomeArquivo)) {
            do {
                i = fin.read();
                if (i != -1) System.out.print((char) i);
            } while (i != -1);
        } catch (IOException exc) {
            System.out.println("Error reading file.");
        }
    }
}

