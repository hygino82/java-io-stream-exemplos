package br.dev.hygino;

import java.io.*;

public class CopiaArquivo {
    public static void main(String[] args) {
        int i;
        String[] arg = {"xanadu.txt", "saida.txt"};
        System.out.println("Copiando o arquivo para: " + arg[1]);
        try (FileInputStream fin = new FileInputStream(arg[0]); FileOutputStream fout = new FileOutputStream(arg[1])) {

            do {
                i = fin.read();
                if (i != -1) fout.write(i);
            } while (i != -1);

        } catch (IOException exc) {
            System.out.println("I/O Error: " + exc);
        }
    }
}
