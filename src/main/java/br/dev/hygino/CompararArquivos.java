package br.dev.hygino;

import java.io.*;

public class CompararArquivos {
    public static void main(String[] args) {
        String[] arquivos = {"xanadu.txt", "saida.txt"};
        int i = 0, j = 0;

        try (FileInputStream f1 = new FileInputStream(arquivos[0]);
             FileInputStream f2 = new FileInputStream(arquivos[1])) {
            do {
                i = f1.read();
                j = f2.read();
                if (i != j) break;
            } while (i != -1 && j != -1);
            if (i != j) {
                System.out.println("Os Arquivos são diferentes.");
            } else {
                System.out.println("Os arquivos são iguais.");
            }
        } catch (IOException exc) {
            System.out.println("I/O Error: " + exc);
        }
    }
}
